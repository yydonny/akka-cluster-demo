package yangyd.cluster.demo1

import com.typesafe.config.ConfigFactory

object Main {
  def main(args: Array[String]): Unit = args.headOption match {
    case Some(mode) ⇒ mode match {
      case "front" ⇒
        Frontend.startup(2777)
        Frontend.startup(0)
        Frontend.startup(0)
      case "back" ⇒
        Backend.startup(3777)
        Backend.startup(0)
      case _ ⇒
        throw new IllegalArgumentException("front or back")
    }
    case _ ⇒
      println(ConfigFactory.load())
  }
}
