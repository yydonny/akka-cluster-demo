package yangyd.cluster.demo1

import akka.actor.{Actor, ActorLogging, ActorSystem, Props, RootActorPath}
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.cluster.{Cluster, Member, MemberStatus}
import com.typesafe.config.ConfigFactory

class Backend extends Actor with ActorLogging {

  import Messages._

  val cluster = Cluster(context.system)

  // subscribe to cluster changes, MemberUp
  // re-subscribe when restart
  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive: Receive = {
    // the actual transformation work
    case TransformationJob(text) ⇒
      sender ! TransformationResult(text.toUpperCase)

    // subscribes to cluster events to detect new, potential, frontend nodes,
    // and send them a registration message so that they know that they can use the backend worker.
    case state: CurrentClusterState ⇒
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) ⇒ register(m)
  }

  def register(target: Member): Unit = {
    if (target.hasRole("frontend")) {
      val frontend = context.actorSelection(RootActorPath(target.address) / "user" / "frontend")
      log.info("=========== registering to {}", frontend)
      frontend ! BackendRegistration
    }
  }
}

object Backend {
  def startup(port: Int): Unit = {
    val config = ConfigFactory.parseString(
      s"""
        akka.remote.netty.tcp.port=$port
        akka.remote.artery.canonical.port=$port
        akka.cluster.roles = [backend]
        """)
        .withFallback(ConfigFactory.load())
    ActorSystem("ClusterSystem", config).actorOf(Props[Backend], name = "backend")
  }
}
